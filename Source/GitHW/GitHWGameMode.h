// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GitHWGameMode.generated.h"

UCLASS(minimalapi)
class AGitHWGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGitHWGameMode();
};



